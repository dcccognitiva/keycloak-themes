<#import "template.ftl" as layout>
<@layout.registrationLayout displayMessage=false; section>
    <#if section = "title">
    ${msg("termsTitle")}
    <#elseif section = "header">
    ${msg("termsTitleHtml")}
    <#elseif section = "form">
    <div id="kc-terms-text" style="height: 60vh; overflow-y: scroll; color: #242E42; text-align: justify;margin-top:20px;">
        <p>A los Usuarios ¨USUARIO¨ les informamos de los siguientes Términos y Condiciones de Uso, los cuales le son aplicables por el simple uso o acceso a cualquiera 
        de los servicios y/o productos brindados por nuestra compañía COGNITIVA ESPAÑA S.L. ¨COGNITIVA" info@cognitiva.la, por lo que entenderemos que los acepta, y 
        acuerda en obligarse en su cumplimiento.</p>
        <p>La aplicación AVIGO de aquí en adelante referida como ¨AVIGO¨ es una solución que corre sobre una plataforma y/o sitio propiedad de COGNITIVA.</p>
        <p>En el caso de que no esté de acuerdo con los Términos y Condiciones de Uso deberá abstenerse de acceder o utilizar nuestros servicios y/o productos.</p>
        <p>Nos reservamos el derecho de modificar estos Términos y Condiciones.</p>

        <h5>INFORMACIÓN GENERAL PARA EL USUARIO</h5>
        <p>COGNITIVA ha desarrollado la aplicación en línea AVIGO que le permite a desarrolladores, individuos, empresas y agencias construir fácilmente, configurar 
        y administrar asistentes virtuales inteligentes. En este documento se detallan los Términos y Condiciones que limitan el uso de nuestra plataforma, productos, 
        servicios y aplicaciones. COGNITIVA es una empresa que proporciona soluciones de computación cognitiva o inteligencia artificial de IBM para ayudar a sus 
        clientes y/o público en general en América Latina y el Caribe a transformar sus industrias e impulsar negocios centrados en datos. De igual forma, COGNITIVA 
        proporciona soluciones a sus clientes para que terceros puedan utilizar todo tipo de datos ingresados voluntariamente por ellos y parametrizar, evaluar, 
        analizar y/o reflejar en distintas herramientas de medición los mismos.</p>

        <h5>DEFINICIONES</h5>
        <p>AVIGO: es un desarrollo de COGNITIVA que utiliza la inteligencia artificial para responder consultas frecuentes de usuarios a través de canales digitales. 
        Este producto atiende las consultas que llegan por el chat de un sitio web o Facebook Messenger de forma totalmente automática. Con esto, dueños y 
        administradores de empresas pueden crear su propio asistente virtual desde su computador y sin necesidad de tener conocimientos técnicos, en modalidad de 
        auto-servicio. </p>
        <p>Usuario: es la persona que ingresa a la plataforma de AVIGO para hacer uso de la tecnología, en modalidad de auto-servicio, y lo configura acorde a sus 
        necesidades de negocio para luego disponibilizar el servicio a través de la integración con su página web o “fanpage” de Facebook.</p>
        <p>Visitante: es la persona que ingresa a través de la página web o Facebook Messenger de un negocio e interactúa con los servicios de chat de AVIGO.</p>
        <p>Sitio y/o Plataforma: es el ambiente o entorno de software común en el cual se desenvuelve la programación de AVIGO y su grupo definido de aplicaciones. 
        El usuario puede crear nuevos asistentes virtuales inteligentes desde la plataforma, así como personalizarlos acorde a su negocio, seleccionar la opción de 
        recarga deseada y hacer uso del código de integración para página web o Facebook Messenger.</p>
        <p>Contenido: se refiere a todo el conjunto de información que se ingrese a la plataforma para personalizar un asistente virtual inteligente, incluyendo 
        aunque no limitándose al texto, los documentos, las descripciones, los productos, el software, el código, gráficos, diseño, fotografía, sonido, videos, 
        funciones interactivas, servicios, compilación de datos y ordenamiento de datos, y todo otro contenido en la Plataforma.</p>
        <p>Cuenta de Usuario: para que el usuario pueda obtener seguridad, acceso al sistema, administración de recursos, etc, dicho usuario deberá identificarse. 
        El usuario necesita una cuenta (una cuenta e usuario) y un usuario, asociados a una contraseña. El usuario utiliza una interfaz de registro para acceder a 
        los sistemas, el proceso de identificación es conocido como identificación de usuario o acceso del usuario al sistema.</p>
        <p>Operador: Un usuario podrá autorizar a terceras personas a operar su Cuenta de Usuario como “Operador” con el alcance que determine ese usuario. El acceso 
        del operador a la plataforma está limitado por el usuario, y su continuidad depende del mismo.</p>
        <p>Periodo de Prueba: se refiere al periodo de tiempo en el cuál COGNITIVA podría disponibilizar funcionalidades en la plataforma, las cuáles se le permitirá 
        utilizar al Usuario sin cargo durante algunos días corridos. Al finalizar el plazo de prueba, para seguir utilizando la funcionalidad y acceder a su información, 
        deberá realizar la compra de un paquete de preferencia. </p>

        <h5>OBJETIVO</h5>
        <p>La aceptación de los siguientes "Términos y Condiciones de Uso" se origina en el interés del USUARIO en utilizar AVIGO, solución de la empresa COGNITIVA.</p>
        <p>Ningún uso por parte del USUARIO constituirá una relación comercial exclusiva con COGNITIVA. El uso por parte del USUARIO servirá para el desarrollo de 
        ciertas capacidades tecnológicas, utilizando los datos que aporta EL USUARIO y otros consumidores de la aplicación AVIGO para mejorar el servicio basado en 
        retroalimentación real. </p>

        <h5>OBLIGACIONES DEL USUARIO</h5>
        <p>EL USUARIO tendrá las siguientes obligaciones, además de las que se establecen en otras disposiciones de estos términos y condiciones o de la ley aplicable:</p>
        <p>a.	Utilizar lenguaje apropiado, por lo que queda totalmente prohibido, bajo pena de terminación del acceso a la plataforma, el uso de leguaje ofensivo, soez, 
        vulgar, discriminatorio o que de cualquier manera atente contra derechos de terceros.</p>
        <p>b.	Garantizar a COGNITIVA que sus comentarios son propios y de ninguna manera afectan derechos de terceros, incluyendo, entre otros, derechos de propiedad intelectual.</p>
        <p>c.	Garantizar que no realizará utilización indebida o ilegítima de Datos Personales, Datos Sensibles, Datos Confidenciales o cualquier otra informacion, así como obras 
        protegidas por Derechos de Autor o otros activos protegidos por derechos de Propiedad Intelectual, de acuerdo con la normativa vigente aplicable.</p>
        <p>d.	Respetar la propiedad intelectual de COGNITIVA y de terceros.</p>

        <h5>CAPACIDAD PARA ACEPTAR LOS TÉRMINOS Y CONDICIONES.</h5>
        <p>EL USUARIO confirma que es mayor de 18 años de edad, y que comprende y acepta que el uso de la aplicación AVIGO no está destinado a menores de 18 años. Si 
        llegase a conocimiento de COGNITIVA que un Usuario es menor de 18 años, COGNITIVA anulará su cuenta de usuario y/o su acceso al Sitio y/o la Plataforma. </p>
        <p>Si EL USUARIO utiliza el Sitio y/o la Plataforma en nombre de una entidad de existencia ideal, por ejemplo una empresa, usted afirma y garantiza que 
        tiene las facultades o las autorizaciones necesarias para actuar en nombre y representación de dicha entidad.</p>

        <h5>DE LA PROPIEDAD INTELECTUAL Y RELEVO DE RESPONSABILIDAD</h5>
        <p>IBM se reserva todos los derechos, títulos e intereses sobre los Servicios de IBM Watson (Incluyendo los algoritmos de aprendizaje automático), Materiales 
        de Desarrollo de IBM y Materiales de Diseño de IBM (Colectivamente denominados "Materiales IBM").  COGNITIVA podrá coadyuvar en la defensa y tutela de dichos 
        derechos, títulos e intereses en caso de que el abuso sobre los mismos le genere algún daño o perjuicio. EL USUARIO no podrá utilizar la propiedad intelectual 
        de COGNITIVA, de IBM o IBM Watson, sin previa autorización escrita de COGNITIVA. No se otorga licencia o derecho alguno sobre Derechos de Autor ni Propiedad 
        Industrial de forma directa o indirecta o a través de terceros de COGNITIVA o IBM. </p>
        <p>COGNITIVA no será responsable en medida alguna, si resultare que los comentarios y/o información aportados por EL USUARIO, en todo o en parte violentan 
        derechos de propiedad intelectual de terceros y/o constituyen algún delito. EL USUARIO libera a COGNITIVA de cualquier obligación surgida de reclamos judiciales 
        o extrajudiciales o realizada por cualquier autoridad competente en relación a la publicación de sus comentarios, y se compromete a mantenerlo indemne, ahora y 
        en el futuro, ante cualquier reclamo indemnizatorio que cualquier tercero haya interpuesto o interponga contra COGNITIVA, sea este judicial, extrajudicial o 
        ante cualquier autoridad competente, tanto nacional como internacionalmente.</p>
        <p>Asimismo, todos los contenidos del Sitio y de la Plataforma -incluyendo aunque no limitándose al texto, los documentos, las descripciones, los productos, el 
        software, el código, gráficos, diseño, fotografía, sonido, videos, funciones interactivas, servicios, compilación de datos y ordenamiento de datos, y todo otro 
        contenido en el Sitio y en la Plataforma- en adelante, el “Contenido”, y las marcas registradas, marcas de servicio y logos incluidos en el Sitio y en la 
        Plataforma (“marcas”), son propiedad de COGNITIVA o están licenciados a COGNITIVA.</p>
        <p>El Contenido se le proporciona “AS IS” (tal como está) para su información y su uso personal únicamente y no podrá ser utilizado, copiado, distribuido, 
        transmitido, emitido, desplegado, vendido, licenciado, descompilado o explotado para cualquier otro propósito sin el consentimiento previo y por escrito de 
        COGNITIVA. COGNITIVA se reserva todos los derechos que no estén expresamente otorgados a Usted en la Plataforma o en el Sitio. Si Ud. descarga o imprime 
        una copia del Contenido para su uso personal, deberá retener todos los derechos reservados y otros avisos propietarios que allí se encuentren.</p>
        <p>Esta sección se mantendrá vigente a pesar de la eventual terminación de estas Condiciones.</p>

        <h5>LÍMITE DE RESPONSABILIDAD.</h5>
        <p>COGNITIVA no será responsable por pérdida de datos o información del USUARIO por el uso indebido de la aplicación.</p>

        <h5>LIMITACIONES AL ACCESO Y AL USO DE LA PLATAFORMA.</h5>
        <p>a. De acuerdo con los términos establecidos en las presentes Condiciones, COGNITIVA otorga al USUARIO durante el plazo correspondiente una licencia 
        limitada, no exclusiva, no sublicenciable e intransferible para acceder y hacer uso de la Plataforma AVIGO en la medida permitida en las presentes Condiciones. 
        Todos los derechos que no se conceden expresamente en las presentes Condiciones están reservados por COGNITIVA.</p>
        <p>b. EL USUARIO no podrá hacer ningún uso de la Plataforma que no esté expresamente permitido en las presentes Condiciones. </p>
        <p>c. Excepto que se indique expresamente lo contrario en estas Condiciones, EL USUARIO no podrá: (a) descargar, usar, copiar, crear obras derivadas, 
        o modificar la Plataforma o cualquier parte de esta, (b) transferir, sublicenciar, arrendar, prestar, alquilar o distribuir la Plataforma a cualquier 
        otra persona o entidad, o (c) usar la Plataforma de manera ilegal, para cualquier fin ilegal, o de cualquier manera no reconocida expresamente en las 
        presentes Condiciones.</p>
        <p>d. COGNITIVA se reserva el derecho exclusivo, a su solo criterio, de: (a) determinar las funciones, servicios, productos, software u otras herramientas 
        que estarán disponibles para el uso del Usuario a través del Sitio y/o de la Plataforma, y (b) agregar, modificar o eliminar cualquier contenido, material 
        y/o funcionalidad que esté disponible en la Plataforma y/o en el Sitio en cualquier momento y por cualquier razón. En este sentido, COGNITIVA puede realizar 
        actualizaciones, nuevas versiones, agregar nuevas funcionalidades y mejoras a la Plataforma y/o al Sitio.</p>
        <p>e. COGNITIVA permitirá el uso de la Plataforma y/o del Sitio, siempre y cuando: (I) EL USUARIO no copie, distribuya o modifique ninguna parte de la 
        Plataforma ni del Sitio sin previa autorización por escrito de COGNITIVA; (II) EL USUARIO no envíe spam, publicidad, correo no deseado, cadenas, etc.; 
        (III) EL USUARIO no transmita ningún contenido que incluya virus de software u otros programas, códigos o archivos que puedan dañar computadoras; (IV) 
        EL USUARIO no desconecte o interrumpa servidores o redes conectados al Sitio; y (V) EL USUARIO cumpla con estos Términos y Condiciones. EL USUARIO 
        presta su acuerdo a no usar o lanzar cualquier sistema automatizado, incluyendo sin limitación "robots," "spiders" o “lectores offline”, que acceden a 
        la Plataforma y/o al Sitio de una manera que puedan mandar más mensajes de pedido a los servidores de COGNITIVA durante un período dado de tiempo que 
        los que un ser humano podría producir razonablemente durante el mismo período utilizando un buscador de web online convencional. COGNITIVA permite a 
        los operadores de motores públicos de búsqueda el uso de “spiders” para copiar materiales del Sitio y/o de la Plataforma con el único propósito de 
        crear índices de materias ubicables, disponibles y pasibles de ser encontrados, pero no ‘cachés’ o archivos de dichos materiales. COGNITIVA se reserva 
        el derecho de revocar estas excepciones tanto en general como en casos específicos. EL USUARIO presta su acuerdo respecto de no colectar o almacenar 
        cualquier información de identidad personal, incluyendo nombres de cuentas o direcciones correo electrónico del Sitio y/o de la Plataforma, ni de usar 
        los sistemas de comunicaciones provistos por el Sitio y/o por la Plataforma para cualquier finalidad comercial que exceda el alcance específicamente y 
        expresamente determinado en estas Condiciones. </p>
        <p>f. Cuentas de USUARIO. Operadores. Responsabilidad Personal del Usuario. Para poder tener acceso a la Plataforma, EL USUARIO deberá acceder a su 
        Cuenta de Usuario de la Plataforma de AVIGO (la “Cuenta de Usuario”). Usted no podrá utilizar de ninguna manera una Cuenta de Usuario que pertenezca 
        a otra persona, sin autorización expresa y por escrito del USUARIO correspondiente. Un USUARIO podrá autorizar a terceras personas a operar su Cuenta 
        de Usuario como “Operador” con el alcance que determine ese USUARIO. Cada USUARIO titular de una Cuenta de Usuario en COGNITIVA es responsable ante 
        COGNITIVA por la actividad de todos y cualquiera de los Operadores de su Cuenta de Usuario de COGNITIVA que hayan sido autorizados para desempeñar 
        algunas o todas las funciones que provee la Plataforma. Cada Operador acepta estas Condiciones y las Políticas de Privacidad al ingresar y usar la 
        Plataforma como Operador, en base a la contraseña de Operador vinculada a una Cuenta de Usuario. Cada Usuario será el único responsable de toda la 
        actividad que tenga lugar en su Cuenta de Usuario, y deberá mantener su contraseña protegida y segura, incluyendo las contraseñas de Operador que 
        pudiera haber creado el Usuario. Si EL USUARIO crea una Cuenta de Usuario en nombre de una persona jurídica, por ejemplo una empresa, EL USUARIO 
        garantiza que tiene la autoridad para obligar a dicha entidad jurídica y que su aceptación de estas Condiciones se considerará como la aceptación por 
        dicha persona jurídica. Cualquier brecha de seguridad o uso indebido que se produzca en su Cuenta de Usuario deberá ser notificados de inmediato a COGNITIVA. 
        EL USUARIO será el único responsable por cualquier uso que se haga de su Cuenta de Usuario o de su contraseña de acceso a la Cuenta de Usuario y de las 
        pérdidas de COGNITIVA u otros debido a dicho uso no autorizado. COGNITIVA no se hará responsable por sus pérdidas o daños o por pérdidas o daños de 
        terceros a causa de cualquier uso indebido o no autorizado de su Cuenta de Usuario. </p>
        <p>Evolución de la PLATAFORMA. COGNITIVA continuará desarrollando mejoras a la PLATAFORMA y sitio y pondrá a disposición del USUARIO funcionalidades 
        nuevas o mejoradas sin costo adicional. COGNITIVA se reserva el derecho de hacer disponible nuevos servicios, funcionalidades y analítica avanzada 
        por un costo adicional. El USUARIO renuncia al derecho de exigir dichas mejoras si no está dispuesto a pagar por ellas.</p>
        <p>g. Cancelación del acceso al Sitio y de Cuentas de Usuario. COGNITIVA, a su sola discreción, tiene el derecho de cancelar su acceso al Sitio y/o a 
        la Plataforma y de impedirle su uso de inmediato, así como de cancelar y/o suspender su Cuenta de Usuario, con y sin causa. Se otorgará un plazo no mayor 
        a 30 días para solicitar la recuperación de la información histórica, luego de este plazo, COGNITIVA se reserva el derecho a destruir la información.</p>
        <p>i. Prohibiciones. EL USUARIO no puede acceder o utilizar la Plataforma y/o el Sitio para cualquier otro fin distinto del específicamente establecido 
        en estas Condiciones. EL USUARIO acepta que, entre las actividades prohibidas, están incluidas (pero no limitadas a) las siguientes: a. Actividad delictiva 
        o que viole las leyes aplicables de cualquier modo. b. La recopilación de datos o de cualquier contenido de la Plataforma para crear cualquier compilación 
        o base de datos, sin el permiso escrito de COGNITIVA. c. Engañar o defraudar a la compañía o a otros Usuarios de la Plataforma de cualquier modo y, en 
        particular, con el objeto de obtener información confidencial. d. Hacer uso indebido de los servicios de soporte que provee COGNITIVA o presentar falsas 
        denuncias. e. Utilizar cualquier información obtenida a través de la Plataforma y/o del Sitio con el fin de acosar, molestar, abusar, dañar o de cualquier 
        otro modo afectar los derechos de terceros. f. Utilizar la Plataforma de cualquier modo con el propósito de competir con COGNITIVA. g. Descifrar, decompilar, 
        desensamblar o utilizar ingeniería inversa sobre cualquier software o código que conforma la Plataforma y/o el Sitio, o permitir o autorizar a cualquier otra 
        persona o entidad para hacerlo. h. Eludir de cualquier modo cualquier medida diseñada para impedir o restringir el acceso a la Plataforma y/o al Sitio.</p>

        <h5>SEGURIDAD DE LA INFORMACIÓN Y CONFIDENCIALIDAD</h5>
        <p>COGNITIVA garantiza la integridad del uso de los datos del USUARIO. Asimismo, los datos requeridos para acceder a la aplicación son: nombre completo, 
        apellido completo y correo electrónico. Dichos usuarios no serán públicos y solamente serán utilizados para evitar la repetición de USUARIOS.</p>
        <p>Asimismo, el USUARIO acepta, reconoce y consiente con la mera utilización de la aplicación, que COGNITIVA podrá explotar comercialmente los datos 
        ingresados por el USUARIO y toda la información que de ellos se derive, luego de ser desasociada y anonimizada, y acepta expresamente la transferencia 
        de dichos datos a teceros, ubicados tanto en el país como en el extranjero</p>
        <p>Toda la información que el USUARIO ingrese en la plataforma, cargue, comparta, guarde, o que provea a través del uso del servicio, incluyendo cualquier 
        Asistente Virtual Inteligente que cree en la PLATAFORMA, podrá ser accesado para propósitos de mejorar el servicio. Cualquier personalización que se genere 
        en la PLATAFORMA le da derecho a COGNITIVA de utilizarla para beneficio de otros usuarios, conservando las consideraciones establecidas en la Política de 
        Privacidad. </p>
        <p>COGNITIVA se reserva el derecho de hacer uso de la información generada por el consumo de cualquier AVIGO creado desde su PLATAFORMA AVIGO, manteniendo 
        las consideraciones de la Política de Privacidad, y de forma desasociada y anónima.</p>

        <h5>DISPOSICIONES GENERALES</h5>
        <p>El USUARIO entiende y acepta que no se están solicitando datos personales, más allá de su nombre y correo electrónico y que ello se hace con la finalidad 
        de poder crear una Cuenta de USUARIO, acceder al sitio y/o plataforma. Fa y mantener la relación comercial entre COGNITIVA y el USUARIO. </p>
        <p>EL USUARIO puede deshabilitar la aplicación en cualquier momento sin dar aviso previo a COGNITIVA.</p>
        <p>El USUARIO no asumirá ni creará ninguna obligación en nombre de COGNITIVA o hará ninguna declaración o garantía sobre COGNITIVA, distintas a las autorizadas 
        por escrito por COGNITIVA.</p>
        <p>En caso de que un tribunal o autoridad judicial o administrativa declare la nulidad de una o más cláusulas de las presentes Condiciones, tal nulidad no 
        afectará el resto de las estipulaciones, las que subsistirán y producirán respecto de las partes todos los efectos por ellas deseados.</p>
        <p>En caso de que un tribunal o autoridad judicial o administrativa declare la nulidad de una o más cláusulas de las presentes Condiciones, tal nulidad no 
        afectará el resto de las estipulaciones, las que subsistirán y producirán respecto de las partes todos los efectos por ellas deseados. </p>
        <p>Si alguna cláusula de las presentes Condiciones fuera declarada nula, el USUARIO involucrado y COGNITIVA harán sus mejores esfuerzos para sustituir o 
        modificar la cláusula respetando el espíritu de la presente.</p>
        <p>Cualquier controversia que se suscite en torno a la existencia, validez, interpretación o cumplimiento de la presente será sometida a las leyes de Costa Rica, 
        con renuncia a cualquier otro fuero o jurisdicción que pudiera corresponderles. </p>
        <p>Salvo que se indique lo contrario, cualquier notificación hacia COGNITIVA deberá realizarse por medio de correo electrónico a la dirección de correo 
        electrónico info@cognitiva.la.</p>
        <p>Todas las comunicaciones dirigidas al USUARIO serán dirigidas a la dirección de correo electrónico que proporcionó durante el proceso de registro.</p>
        <p>A los efectos de estas Condiciones, las notificaciones que haga COGNITIVA por medio de correo electrónico se considerarán como recibidas, leídas y aceptadas 
        24 horas después de enviado dicho correo, a menos que  COGNITIVA sea expresamente anoticiado de que la dirección de correo electrónico es inválida o de que el 
        correo electrónico no ha llegado a destino. </p>
        <p>Estas Condiciones, con el alcance establecido en estas Condiciones, constituye el acuerdo completo entre el USUARIO y COGNITIVA con respecto al uso de la 
        Plataforma y/o Sitio. </p>
        <p>El fracaso o la omisión de COGNITIVA para ejercer o hacer valer cualquier derecho o disposición establecido en estas Condiciones no constituirá una renuncia 
        a tal derecho o disposición. </p>
        <p>Los títulos de las secciones en estas Condiciones son por simple conveniencia y no tienen ningún efecto legal o contractual.</p>
        <p>El USUARIO y COGNITIVA acuerdan que cualquier causal de acción que surja de o esté relacionada con la plataforma  y/o sitio deberá comenzar dentro de 1 
        (un) año desde la ocurrencia. De otra forma, dicha causal de acción será declarada sin efecto. Algunas jurisdicciones no permiten la limitación contractual 
        aquí establecida, por lo que podría no ser aplicable.</p>

        <h5>MODIFICACIONES A LA PLATAFORMA/SITIO.</h5>
        <p>COGNITIVA se reserva el derecho de, en cualquier momento, modificar o discontinuar, temporal o permanentemente, la Plataforma y/o Sitio (o cualquier parte) 
        con o sin previo aviso. El USUARIO acepta que COGNITIVA no será responsable ante usted ni ante ningún tercero por cualquier modificación, suspensión o 
        interrupción definitiva de la Plataforma y/o Sitio.</p>

        <h5>LIMITACIÓN GENERAL DE RESPONSABILIDAD Y GARANTÍA.</h5>
        <p>Salvo si se pacta expresamente de otra manera en un acuerdo por escrito entre EL USUARIO y COGNITIVA, la plataforma y/o sitio se provee "as is" (tal como 
        está), según su disponibilidad y sin ningún tipo de garantía, incluyendo sin limitación, representaciones, garantías y condiciones de comercialización, calidad 
        comercial, adaptación a un propósito determinado, título, no-infracción, y todo lo que surja por estatuto o por transcurso de tratamiento o uso de marca. </p>
        <p>Si bien hacemos esfuerzos razonables para asegurar que el sitio funcione como se espera que lo haga, COGNITIVA no garantiza que el sitio esté libre de 
        errores, brechas de seguridad, ataques de virus y demás, o que siempre estará disponible. El sitio en ocasiones estará indisponible por razones de mantenimiento 
        de rutina, actualizaciones o por otros motivos.</p>
        <p>Además, EL USUARIO acuerda que COGNITIVA no se hará responsable por ninguna consecuencia para con sus usuarios que podrían surgir a causa de problemas técnicos 
        de internet, lentitud en las conexiones, congestión en el tráfico o sobrecarga de nuestro u otros servidores, etc.</p>
        <p>COGNITIVA no garantiza, endosa, promociona o asegura ningún contenido, producto o servicio que aparezca publicado en el sitio.</p>
        <p>Bajo ninguna circunstancia ni COGNITIVA, ni sus filiales y sus repectivos gerentes, directores, empleados, licenciatarios, asignados y agentes se harán 
        responsables por el extravío de dinero, bienes, reputación, daños especiales, indirectos, directos, incidentales, punitivos o consecuenciales que resulten del 
        uso, o de la imposibilidad de dicho uso y de sus servicios aun si tendencias o noticias hayan avisado de la posibilidad de que estos daños se produzcan.</p>

        <h5>TERMINACIÓN.</h5>
        <p>COGNITIVA podrá dar por terminado el acceso y uso del USUARIO de forma inmediata y de pleno derecho, con justa causa, sin ninguna responsabilidad de su 
        parte y con derecho a cobrar daños y prejuicios, tomando a su vez todas las acciones que la legislación aplicable le faculta, en los siguientes casos:</p>
        <p>a.	Ante una infracción del USUARIO, o una violación, o un incumplimiento de sus obligaciones, términos o condiciones aquí establecidos.</p>
        <p>b.	Por cualquier conducta o práctica de EL USUARIO que a juicio de COGNITIVA resulte perjudicial para el buen nombre o para la reputación de COGNITIVA, 
        de sus productos y servicios.</p>

        <h5>ACCESO A LA PLATAFORMA.</h5>
        <p>a. El acceso a la plataforma de AVI Go es gratuito, se puede ingresar, registrar y crear cuantos asistentes se necesiten, personalizarlos y configurarlos 
        con las respuestas que aplican específicamente a ese negocio. Cuando se está listo para realizar la integración con canales digitales (Facebook Messenger y 
        sitio web), se debe de adquirir crédito de conversaciones, los cuáles se ofrecen bajo la modalidad de pre-pago por consumo (flexible o en paquetes) de acuerdo 
        a las tarifas publicadas en el Sitio. </p>
        <p>b. No obstante lo establecido en la cláusula a. de las presentes Condiciones, COGNITIVA podrá habilitar períodos de prueba, con o sin limitaciones de 
        funcionalidades, en los que se permitirá utilizar al Usuario sin cargo la Plataforma durante algunos días corridos. Al finalizar el plazo de prueba, para 
        seguir utilizando la Plataforma y acceder a su información, deberá realizar la compra de un paquete de preferencia.</p>
        <p>c.	Para acceder a los servicios de la Plataforma –sin perjuicio de lo previsto en la cláusula b.-, EL USUARIO deberá realizar la compra de consumo 
        deseado por adelantado, a través de las opciones de pago incluídas en la Plataforma a la tarifa establecida o planes promocionales que de tanto en tanto 
        sean ofrecidos por COGNITIVA a los Usuarios, mediante el medio de pago seleccionado y los términos y condiciones aplicables, con los términos y condiciones 
        aplicables. Esta acción es requerida antes de poder completar la integración del producto en sus canales digitales.</p>
        <p>d.	Los pagos deben realizar por adelantado al consumo del servicio. COGNITIVA se reserva la facultad de modificar, a su exclusivo criterio, los precios 
        publicados en el Sitio y/o en la Plataforma así como la denominación, la clasificación, los requisitos y las funcionalidades de cada variante de los servicios 
        correspondientes a cada precio publicado en su página web www.asistentevirtualinteligente.com</p>
        <p>d. Medios de pago. AVIGO acepta pagos a través de su Plataforma. EL USUARIO se compromete a pagar a COGNITIVA  todos los gastos por los servicios contratados 
        a COGNITIVA mediante el uso de la Plataforma. Al agotarse todo el consumo disponible en la Plataforma, se le enviará una notificación al USUARIO para recordarle 
        realizar una recarga. En caso de que no se realice una recarga antes de que el consumo disponible alcance 0 (cero), el servicio se bloqueará hasta que se recargue 
        de nuevo. La baja del servicio por falta de pago en tiempo y forma es automática.</p>
        <p>e. La Plataforma estará disponible para EL USUARIO de forma gratuita, sin embargo sólo podrá integrar el servicio a sus canales digitales desde el momento en 
        que COGNITIVA verifique la compra efectuada por EL USUARIO. En caso que la compra no pueda ser verificada por COGNITIVA, por cualquier motivo, COGNITIVA podrá, 
        a su exclusiva discreción, decidir la no habilitación del servicio por parte del USUARIO para el período imputado a esa compra, hasta tanto COGNITIVA pueda 
        verificar la compra realizada por el USUARIO. En caso de no verificación de la compra, COGNITIVA le notificará al USUARIO por correo electrónico esta situación.</p>
        <p>g. Los pagos realizados por EL USUARIO a COGNITIVA no son reembolsables en ningún caso, tanto si la provisión del servicio es cancelada o terminada por EL USUARIO 
        o por COGNITIVA, por cualquier razón o causa.</p>

        <h5>NULIDAD PARCIAL</h5>
        <p>Si cualquier disposición establecida en los presentes términos y condiciones resultara ser nula, inválida, ineficaz o inaplicable, ello no afectará la validez, eficacia 
        y aplicabilidad de las otras disposiciones de  estos términos y condiciones.</p>

        <h5>PLAZO</h5>
        <p>Los presentes términos y condiciones rigen durante todo el tiempo en que el USUARIO utilice los productos y/o servicios de COGNITIVA. COGNITIVA se reserva el derecho de 
        cambiar los mismos, los que serán publicados en esta misma pagina. </p>

        <h5>JURISDICCION</h5>
        <p>Para la interpretación, cumplimiento y ejecución de los presentes Términos y Condiciones de Uso, el USUARIO está de acuerdo en que serán aplicables las leyes 
        de Costa Rica, renunciando expresamente a cualquier otro fuero o jurisdicción que pudiera corresponderles debido a sus domicilios presentes o futuros o por 
        cualquier otra causa.</p>
    </div>
    <form class="form-actions" action="${url.loginAction}" method="POST">
        <input class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" name="accept" id="kc-accept" type="submit" value="${msg("doAccept")}" style="font-weight: 400 !important;"/>
        <input class="${properties.kcButtonClass!} ${properties.kcButtonDefaultClass!} ${properties.kcButtonLargeClass!} btn-decline" name="cancel" id="kc-decline" type="submit" value="${msg("doDecline")}" style="font-weight: 400 !important;"/>
    </form>
    </#if>
</@layout.registrationLayout>