<#macro registrationLayout bodyClass="" displayInfo=false displayMessage=true>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" class="${properties.kcHtmlClass!}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow">

    <#if properties.meta?has_content>
        <#list properties.meta?split(' ') as meta>
            <meta name="${meta?split('==')[0]}" content="${meta?split('==')[1]}"/>
        </#list>
    </#if>
    <title><#nested "title"></title>
    <link rel="icon" href="${url.resourcesPath}/img/favicon.ico" />
    <#if properties.styles?has_content>
        <#list properties.styles?split(' ') as style>
            <link href="${url.resourcesPath}/${style}" rel="stylesheet" />
        </#list>
    </#if>
    <#if properties.scripts?has_content>
        <#list properties.scripts?split(' ') as script>
            <script src="${url.resourcesPath}/${script}" type="text/javascript"></script>
        </#list>
    </#if>
    <#if scripts??>
        <#list scripts as script>
            <script src="${script}" type="text/javascript"></script>
        </#list>
    </#if>
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://momentjs.com/downloads/moment.js" type="text/javascript"></script>
</head>

<body class="${properties.kcBodyClass!}">

    <div id="kc-container" class="col-xs-12 col-sm-6 col-md-6 col-lg-6 login col-md-offset-3 col-lg-offset-3 col-sm-offset-3" style="margin-bottom: 60px;">
        <div id="kc-container-wrapper" class="${properties.kcContainerWrapperClass!}">

            <div id="kc-content" class="${properties.kcContentClass!}" style="margin-bottom: 60px;">
                <div id="kc-content-wrapper" class="${properties.kcContentWrapperClass!}">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="logo-cognitiva"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div id="cognitiva-welcome" class="col-md-12 cognitiva-title">BIENVENIDO A TU ASESOR VIRTUAL INTELIGENTE</div>    
                    </div>
                    <#if displayMessage && message?has_content>
                        <div class="${properties.kcFeedbackAreaClass!}">
                            <div class="alert alert-${message.type}">
                                <#if message.type = 'success'><span class="${properties.kcFeedbackSuccessIcon!}" style="color: #242E42 !important;"></span></#if>
                                <#if message.type = 'warning'><span class="${properties.kcFeedbackWarningIcon!}" style="color: #242E42 !important;"></span></#if>
                                <#if message.type = 'error'><span class="${properties.kcFeedbackErrorIcon!}" style="color: #242E42 !important;"></span></#if>
                                <#if message.type = 'info'><span class="${properties.kcFeedbackInfoIcon!}" style="color: #242E42 !important;"></span></#if>
                                <span class="kc-feedback-text" style="color: #242E42 !important;">${message.summary?no_esc}</span>
                            </div>
                        </div>
                    </#if>

                    <div id="kc-form" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 login">
                        <div id="kc-form-wrapper" class="${properties.kcFormAreaWrapperClass!}">
                            <#nested "form">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">
        <p id="footer">AVI Go </p>
    </footer>
    <script>
        document.getElementById("footer").innerHTML += moment().year();
    </script>
</body>
</html>
</#macro>
